<?php

namespace Samarties\Cacher\Exception;

class CacheDataNotFoundException extends \Exception
{
	protected $message = 'No cache data found for key \'%s\'.';
	
	public function __construct($key, $code = 0, \Exception $previous = null)
	{
		parent::__construct(sprintf($this->message, $key), $code, $previous);
	}
}