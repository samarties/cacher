<?php

namespace Samarties\Cacher\Driver;

class XcacheDriver extends AbstractDriver 
{
	/**
	 * @inheritDoc
	 */
	public function save($key, $data, array $tags = array(), $lifeTime = 0)
	{
		if (count($tags)) $this->addTags($key, $tags);
		
		return xcache_set($key, serialize($data), (int) $lifeTime);
	}
	
	/**
	 * @inheritDoc
	 */
	public function contains($key)
	{
		return xcache_isset($key);
	}
	
	/**
	 * @inheritDoc
	 */
	public function fetch($key)
	{
		return $this->contains($key) ? unserialize(xcache_get($key)) : false;
	}
	
	/**
	 * @inheritDoc
	 */
	public function delete($key)
	{
		$deleted = xcache_unset($key);
		if ($deleted) $this->removeKeyFromTags($key);
		
		return $deleted;
	}
	
	/**
	 * @inheritDoc
	 */
	public function flush()
	{
		xcache_clear_cache(XC_TYPE_VAR, 0);
		
		return true;
	}
	
	/**
	 * @inheritDoc
	 * 
	 * @throws \BadMethodCallException If the system cannot access the xcache_list function 
	 */
	public function getList()
	{
		$list = $this->getXCacheList();
		$keys = array();
		foreach($list['cache_list'] as $cacheEntry)
		{
			$keys[] = $cacheEntry['name'];
		}
		return $keys;
	}
	
	/**
	 * Uses the xcache_list function to retrieve information about each cache entry
	 * 
	 * @return array
	 */
	public function getXCacheList()
	{
		if (!$this->isAuthed())
		{
			throw new \BadMethodCallException('To use all of the features of XCache, you must set "xcache.admin.enable_auth" to "Off" in your php.ini.');
		}
		
		return xcache_list(XC_TYPE_VAR, 0);
	}
	
	/**
	 * @inheritDoc
	 */
	public static function isSupported()
	{
		return function_exists('xcache_get');
	}
	
	/**
	 * Determines if xcache is running with administrator access
	 * 
	 * @return boolean
	 */
	protected function isAuthed()
	{
		return !ini_get('xcache.admin.enable_auth');
	}
}

