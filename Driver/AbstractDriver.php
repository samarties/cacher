<?php

namespace Samarties\Cacher\Driver;

abstract class AbstractDriver implements DriverInterface
{
	private $tagsKey = 'samarties.cache.tags';
	
	/**
	 * @inheritDoc
	 */
	public function deleteMultiple(array $keys)
	{
		$deleted = array();
		foreach($keys as $index => $key)
		{
			$deleted[$index] = $this->delete($key);
		}
	
		return $deleted;
	}
	
	/**
	 * @inheritDoc
	 */
	public function deleteWithTags(array $tags)
	{
		$keys = $this->getListWithTags($tags);
		$this->deleteMultiple($keys);
	}
	
	/**
	 * @inheritDoc
	 */
	public function deleteByRegex($pattern)
	{
		$keys = $this->getListByRegex($pattern);
		$this->deleteMultiple($keys);
	}
	
	/**
	 * @inheritDoc
	 */
	public function getListWithTags(array $tags)
	{
		$cachedTags = $this->getTags();
		$list = array();
		foreach($cachedTags as $cachedTag => $cachedKeys)
		{
			if (in_array($cachedTag, $tags))
			{
				$list = array_merge($list, $cachedKeys);
			}
		}
		return $list;
	}
	
	/**
	 * inheritDoc
	 */
	public function getListByRegex($pattern)
	{
		$list = $this->getList();
		$matchingList = array();
		foreach($list as $key)
		{
			if (preg_match($pattern, $key))
			{
				$matchingList[] = $key;
			}
		}
		
		return $matchingList;
	}
	
	/**
	 * Retrieves currently stored tags
	 * 
	 * @return array
	 */
	protected function getTags()
	{
		return $this->contains($this->tagsKey) ? $this->fetch($this->tagsKey) : array();
	}
	
	/**
	 * Stores tags against a given key
	 * 
	 * @param string $key
	 * @param array $tags
	 * 
	 * @return boolean True is tags were successfully saved, false otherwise
	 */
	protected function addTags($key, array $tags)
	{
		$cachedTags = $this->getTags();
		foreach($tags as $tag)
		{
			if (!isset($cachedTags[$tag])) $cachedTags[$tag] = array();
			$cachedTags[$tag][] = $key;
			$cachedTags[$tag] = array_unique($cachedTags[$tag]);
		}
		
		return $this->save($this->tagsKey, $cachedTags);
	}
	
	/**
	 * Removes any instance of the given keys from the tags array
	 * 
	 * @param array<string> $keys
	 * 
	 * @return boolean True is tags were successfully removed, false otherwise
	 */
	protected function removeKeysFromTags(array $keys)
	{
		$cachedTags = $this->getTags();
		foreach($cachedTags as $cachedTag => &$cachedKeys)
		{
			$cachedKeys = array_diff($cachedKeys, $keys);
			
			// remove empty tags
			if (!count($cachedKeys)) unset($cachedTags[$cachedTag]);
		}
		unset($cachedKeys);
		
		return $this->save($this->tagsKey, $cachedTags);
	}
	
	/**
	 * Convenience function to remove a single key from the tags array
	 * 
	 * @see removeKeysFromTags
	 * 
	 * @param string $key
	 */
	protected function removeKeyFromTags($key)
	{
		return $this->removeKeysFromTags(array($key));
	}
}