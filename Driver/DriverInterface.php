<?php

namespace Samarties\Cacher\Driver;

use Samarties\Cacher\Exception\CacheDataNotFoundException;

interface DriverInterface
{
	/**
	 * Stores data in the cache
	 * 
	 * @param string $key The key to store the data against	 
	 * @param mixed $data The data to be saved
	 * @param array $tags<string> Tags associated with this data (can be used to retrieve later)
	 * @param integer $lifeTime The length of time to store the data for (0 = unlimied)
	 * 
	 * @return boolean True is successfully cached, false otherwise
	 */
	public function save($key, $data, array $tags = array(), $lifeTime = 0);
	
	/**
	 * Determines if a key exists
	 * 
	 * @param string $key
	 * 
	 * @return boolean
	 */
	public function contains($key);
	
	/**
	 * Returns the data stored with the given key
	 * 
	 * @param string $key
	 * 
	 * @return mixed
	 * 
	 * @throws CacheDataNotFoundException When the key does not exist
	 */
	public function fetch($key);
	
	/**
	 * Delete a cache entry using the given key
	 * 
	 * @param string $key
	 * 
	 * @return boolean True if successfully delete, false otherwise
	 */
	public function delete($key);
	
	/**
	 * Deletes multiple cache entries. An array is returned contained the results
	 * of each delete in the same order they are given, i.e. the index 0 in the returned
	 * array is the result of the deletion of the key at index 0 of the $keys array. Note
	 * that keys are preserved in the returned array
	 * 
	 * @param array $keys
	 * 
	 * @return array<boolean>
	 */
	public function deleteMultiple(array $keys);
	
	/**
	 * Deletes all entries associated with the given tags.
	 * 
	 * @param array $tags
	 */
	public function deleteWithTags(array $tags);
	
	/**
	 * Deletes all entries whose keys match the given regex pattern
	 *
	 * @param string $pattern
	 */
	public function deleteByRegex($pattern);
	
	/**
	 * Clear the cache
	 * 
	 * @return boolean
	 */
	public function flush();
	
	/**
	 * Returns a list of all stored cache keys
	 * 
	 * @return array<string>
	 */
	public function getList();
	
	/**
	 * Returns a list of all stored cache keys which have one or more
	 * of the given tags
	 * 
	 * @param array $tags
	 * 
	 * @return array<string>
	 */
	public function getListWithTags(array $tags);
	
	/**
	 * Returns a list of all entries whose keys match the given regex pattern
	 *
	 * @param string $pattern
	 * 
	 * @return array<string>
	 */
	public function getListByRegex($pattern);
	
	/**
	 * Determines if this driver is supported by the system
	 * 
	 * @return boolean
	 */
	public static function isSupported();
}

